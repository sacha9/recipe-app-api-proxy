#!/bin/sh

# return any failure error message to the screen
set -e 

# substitute environment variables at this location
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# tells nginx to run in the foreground in a docker container
nginx -g 'daemon off;'